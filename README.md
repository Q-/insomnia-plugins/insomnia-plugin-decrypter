# insomnia-plugin-decrypter

1. Right click on a request folder, set an algorithm.
2. Right click on a request folder, set a secret key of algorithm.
3. Send request, it will decrypt response body if algorithm and secret are provided.
