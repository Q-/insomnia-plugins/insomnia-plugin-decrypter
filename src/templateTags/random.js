module.exports = {
  name: 'random',
  displayName: '隨機整數',
  description: '可以設定最小值 ~ 最大值',
  args: [
      {
          displayName: 'Minimum',
          description: 'Minimum potential value',
          type: 'number',
          defaultValue: 0
      },
      {
          displayName: 'Maximum',
          description: 'Maximum potential value',
          type: 'number',
          defaultValue: 100
      }
  ],
  async run (context, min, max) {
      return Math.round(min + Math.random() * (max - min));
  }
}
