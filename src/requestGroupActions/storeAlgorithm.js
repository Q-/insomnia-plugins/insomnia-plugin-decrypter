module.exports = {
  label: '設定解碼演算法',
  async action({ store, app }, { requestGroup, requests }) {
    store
      .getItem(`algorithm.${requestGroup._id}`)
      .then(algorithm => app.prompt('設定解碼演算法', {
        defaultValue: algorithm || '',
        submitName: '保存',
        cancelable: true,
        hint: '可透過: `$ openssl list-cipher-algorithms` 查詢所有支援的演算法',
        inputType: 'text',
        placeholder: 'AES-256-CBC',
        // validate(str) { return str },
        hints: ['AES-128-CBC', 'AES-192-CBC', 'RC4-40'],
      }))
      .then(algorithm => Promise.all(
        [requestGroup._id, ...requests.map(req => req._id)]
          .map(id => store.setItem(`algorithm.${id}`, algorithm))
      ))
      .catch(err => console.log(`[store algorithm] ${err}`))

    // app.showGenericModalDialog('標題', {
    //   html: `
    //     <h1>Hello</h1>
    //   `
    // })
  },
}