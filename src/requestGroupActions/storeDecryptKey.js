module.exports = {
  label: '設定演算法 Key 值',
  async action({ store, app }, { requestGroup, requests }) {
    const id = requestGroup._id
    Promise.all([
      store.getItem(`algorithm.${id}`),
      store.getItem(`secret.${id}`),
    ])
      .then(([algorithm, secret]) => app.prompt('設定 secret', {
        label: `當前演算法 ${algorithm}`,
        defaultValue: secret,
        submitName: '設定',
        cancelable: true,
        inputType: 'password',
        // hint: '32 bit ',
        // validate(str) { return str },
      }))
      .then(secret => Promise.all(
        [requestGroup._id, ...requests.map(req => req._id)]
          .map(id => store.setItem(`secret.${id}`, secret))
      ))
      .catch(err => console.log(`[store secret] ${err}`))
  },
}