module.exports.requestGroupActions = [
  require('./requestGroupActions/storeAlgorithm'),
  require('./requestGroupActions/storeDecryptKey'),
]

// module.exports.templateTags = [
//   require('./templateTags/random'),
// ]

module.exports.responseHooks = [
  require('./responseHooks/decryptContent'),
]
