module.exports = async function decryptContent({ request, response, store }) {
  const id = request.getId()
  return Promise.all([
    store.getItem(`algorithm.${id}`),
    store.getItem(`secret.${id}`),
  ])
    .then(([algorithm, secret]) => {
      if (!algorithm || !secret) return
      console.log('[decrypt content] 解譯內容...')
      console.dir(response)

      const decipher = require('crypto').createDecipheriv(algorithm, secret, '')
      response.setBody(decipher.update(response.getBody(), 'buffer'))
      return response
    })
}